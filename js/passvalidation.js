var password = document.getElementById("inputPassword")
  , confirm_password = document.getElementById("inputPasswordConfirm");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Senhas não estão iguais. Verifique e corrija, por gentileza.");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;	